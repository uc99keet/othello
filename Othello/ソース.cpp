#include <cstdio>
#include <iostream>
#include <conio.h>

using namespace std;

//定数の定義
const int BOARD_SIZE = 8;
const int EMPTY = 0;
const int BLACK = 1;
const int WHITE = 2;

//盤面クラスの定義
class OthelloBoard {
private:
	int board[BOARD_SIZE][BOARD_SIZE];

public:
	OthelloBoard() {
		//初期盤面の設定
		for (int y = 0; y < BOARD_SIZE; y++) {
			for (int x = 0; x < BOARD_SIZE; x++) {
				board[y][x] = EMPTY;
			}
		}
		board[3][3] = BLACK;
		board[3][4] = WHITE;
		board[4][3] = WHITE;
		board[4][4] = BLACK;
	}

	//盤面の描画
	void display(int cursorX, int cursorY) const {
		system("cls");
		for (int y = 0; y < BOARD_SIZE; y++) {
			for (int x = 0; x < BOARD_SIZE; x++) {
				if (y == cursorY && x == cursorX) {
					cout << "◎ ";
				}
				else {
					switch (board[y][x])
					{
					case 0:cout << "■ "; break;
					case 1:cout << "● "; break;
					case 2:cout << "○ "; break;
					}
				}
			}
			cout << endl;
		}
	}

	//石の反転処理
	int flipStone(int turn, int x, int y, bool doFlip) {
		bool isFlip = false;

		int dx[8] = { 0, 1, 1, 1, 0, -1, -1, -1 };
		int dy[8] = { -1, -1, 0, 1, 1, 1, 0, -1 };

		for (int i = 0; i < 8; i++) {
			int nx = x + dx[i];
			int ny = y + dy[i];
			bool flip = false;

			while (isInBounds(nx, ny) && board[ny][nx] == 3 - turn) {
				nx += dx[i];
				ny += dy[i];
				flip = true;
			}

			if (flip && isInBounds(nx, ny) && board[ny][nx] == turn) {
				if (doFlip) {
					nx -= dx[i];
					ny -= dy[i];

					while (nx != x || ny != y) {
						board[ny][nx] = turn;
						nx -= dx[i];
						ny -= dy[i];
					}
				}
				isFlip = true;
			}
		}

		if (isFlip) {
			if (doFlip) {
				board[y][x] = turn;
			}
			return 1;
		}
		return 0;
	}

	//石を置けるかのチェック
	bool canPlaceStone(int turn) {
		for (int y = 0; y < BOARD_SIZE; y++) {
			for (int x = 0; x < BOARD_SIZE; x++) {
				if (board[y][x] == 0 && flipStone(turn, x, y, false)) {
					return true;
				}
			}
		}
		return false;
	}

	//勝敗判定
	void calculateWinner() const {
		int black = 0, white = 0;
		for (int y = 0; y < BOARD_SIZE; y++) {
			for (int x = 0; x < BOARD_SIZE; x++) {
				if (board[y][x] == 1) black++;
				if (board[y][x] == 2) white++;
			}
		}

		cout << "黒：" << black << "  白：" << white << endl;
		if (black > white) {
			cout << "黒の勝ち！！！" << endl;
		}
		else if (white > black) {
			
			cout << "白の勝ち！！！" << endl;
		}
		else {
			cout << "引き分け！！！" << endl;
		}
	}

private:
	//　範囲チェック
	bool isInBounds(int x, int y) {
		return x >= 0 && x < BOARD_SIZE && y >= 0 && y < BOARD_SIZE;
	}
};


	int main() {
		OthelloBoard board;
		char ch = 0;
		int x = 4, y = 4, turn = BLACK;

		while (ch != 27) { //ESCキーで終了
			board.display(x, y);


			cout << (turn == BLACK ? "BLACK's turn" : "WHITE's turn") << endl;
			cout << "↑W　←A　↓S　→D Spaceキーで石が置けます" << endl;

			if (!board.canPlaceStone(turn)) {
				turn = 3 - turn;
				if (!board.canPlaceStone(turn)) {
					ch = 27;
					board.calculateWinner();
					break;
				}
				else {
					cout << "返せるマスがありません" << endl;
				}
				_getch();
				continue;
			}


			ch = _getch();
			switch (ch) {
			case 'w':if (y > 0) y--; break;
			case 'a':if (x > 0) x--; break;
			case 's':if (y < 7) y++; break;
			case 'd':if (x < 7) x++; break;
			case 32: //スペースキーで石を置く
				if (board.flipStone(turn, x, y, true) == 1) {
					turn = 3 - turn;
				}
				break;
			}
		}
		return 0;
	}
