#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QVBoxLayout>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    BoardWidget *boardWidget = new BoardWidget(ui->centralwidget);
    QVBoxLayout *layout = new QVBoxLayout(ui->centralwidget);
    layout->setContentsMargins(0,0,0,0);
    layout->addWidget(boardWidget);

    // BoardWidget *boardWidget = new BoardWidget(ui->centralwidget);
    // ui->centralwidget->layout()->addWidget(boardWidget);
}

MainWindow::~MainWindow()
{
    delete ui;
}
