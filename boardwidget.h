#ifndef BOARDWIDGET_H
#define BOARDWIDGET_H

#include <QWidget>
#include <QVector>
#include <QPoint>

class BoardWidget : public QWidget
{
    Q_OBJECT

public:
    explicit BoardWidget(QWidget *parent = nullptr);
    enum class PieceColor { None, Black, White };

protected:
    void mousePressEvent(QMouseEvent *event) override;
    void paintEvent(QPaintEvent *event) override;
    bool hasValidMoves(PieceColor color);
    bool isGameOver() const;
    bool isValidMove(int x, int y, PieceColor color) const;
    bool canPlacePiece(PieceColor color) const;

private:
    QVector<QVector<PieceColor>> boardState;
    int cellSize;
    int boardSize;
    PieceColor turn;

    void placePiece(int x, int y);
    PieceColor nextTurn(PieceColor currentTurn) const;
};

#endif // BOARDWIDGET_H
