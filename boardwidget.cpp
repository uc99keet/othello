#include "boardwidget.h"
#include <QPainter>
#include <QLayout>
#include <QPoint>
#include <QMouseEvent>


//ボードの状態を定義する関数
BoardWidget::BoardWidget(QWidget *parent)
    : QWidget(parent), cellSize(60), boardSize(8), turn(PieceColor::Black)
{
    boardState = QVector<QVector<PieceColor>>(boardSize, QVector<PieceColor>(boardSize, PieceColor::None));
    int center = boardSize / 2;
    boardState[center][center] = PieceColor::Black;
    boardState[center - 1][center - 1] = PieceColor::Black;
    boardState[center][center - 1] = PieceColor::White;
    boardState[center - 1][center] = PieceColor::White;
}

//ゲーム終了したかどうかの確認する関数
bool BoardWidget::isGameOver() const
{
    for(int row = 0; row < boardSize; ++row) {
        for(int col = 0; col < boardSize; ++col){
            if(boardState[col][row] == PieceColor::None){
                return false;
            }
        }
    }

    if(!canPlacePiece(PieceColor::Black) && !canPlacePiece(PieceColor::White)){
        return true;
    }
    return false;
}

//石をおけるかをチェックする関数
bool BoardWidget::canPlacePiece(PieceColor color) const
{
    for(int row = 0; row < boardSize; ++row){
        for(int col = 0; col < boardSize; ++col){
            if(boardState[col][row] == PieceColor::None && isValidMove(col, row, color)){
                return true;
            }
        }
    }
    return false;
}

//指定された座標に石がおけるかどうかを確認する関数
bool BoardWidget::isValidMove(int x, int y, PieceColor color) const
{
    if(boardState[x][y] != PieceColor::None){
        return false;
    }
    //指定された座標の８方向に対して相手のマスがあるかチェック
    for(int dx = -1; dx <= 1; ++dx){
        for(int dy = -1; dy <= 1; ++dy){
            if(dx == 0 && dy == 0){
                continue;
            }
            int cx = x + dx;
            int cy = y + dy;
            bool foundOpponent = false;

            while (cx >= 0 && cx < boardSize && cy >= 0 && cy < boardSize) {
                if(boardState[cx][cy] == nextTurn(color)) {
                    cx += dx;
                    cy += dy;
                    foundOpponent = true;
                } else if(boardState[cx][cy] == color){
                    if(foundOpponent){
                        return true;
                    } else {
                        break;
                    }
                } else {
                    break;
                }
            }
        }
    }
    return false;
}

//マウスのクリック時の処理をする関数
void BoardWidget::mousePressEvent(QMouseEvent *event)
{
    QPoint clickPos = event->pos();

    int cellX = clickPos.x() * 8 / width();
    int cellY = clickPos.y() * 8 / height();
    placePiece(cellX,cellY);
    update();
    QWidget::mousePressEvent(event);
}

//ボードの描画する関数
void BoardWidget::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);

    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);

    int cellSize = qMin(width(), height()) / 8;
    for(int row = 0; row < 8; ++row) {
        for(int col = 0; col < 8; ++col){
            QColor color = ((row + col) % 2 == 0) ? QColor(100, 200,100) : QColor(100, 200,100);
            painter.fillRect(col * cellSize, row * cellSize, cellSize, cellSize, color);

            painter.setPen(Qt::black);
            painter.drawLine((col + 1) * cellSize, row * cellSize,(col + 1) * cellSize, (row + 1) * cellSize);
            painter.drawLine(col * cellSize, (row + 1) * cellSize,(col + 1) * cellSize, (row + 1) * cellSize);
            painter.drawLine(col * cellSize, row * cellSize, (col + 1) * cellSize, row * cellSize);
            painter.drawLine(col * cellSize, row * cellSize, col * cellSize, (row + 1) * cellSize);

            PieceColor pieceColor = boardState[col][row];
            if(pieceColor != PieceColor::None){
                QColor pieceFillColor = (pieceColor == PieceColor::Black) ? Qt::black : Qt::white;
                QColor pieceOutlineColor = (pieceColor == PieceColor::Black) ? Qt::white : Qt::black;

                painter.setBrush(pieceFillColor);
                painter.setPen(pieceOutlineColor);
                painter.drawEllipse(col * cellSize + cellSize / 4, row * cellSize + cellSize / 4, cellSize / 2, cellSize / 2);
            }
        }
    }
}

//指定したマス目に石を置く関数
void BoardWidget::placePiece(int x, int y)
{
    if(boardState[x][y] != PieceColor::None){
        return;
    }

    bool canPlace = false;
    QVector<QPoint> flippedPieces;

    for(int dx = -1; dx <= 1; ++dx){
        for(int dy = -1; dy <= 1; ++dy){
            if(dx == 0 && dy == 0){
                continue;
            }
            int cx = x + dx;
            int cy = y + dy;
            QVector<QPoint> flippedPiecesInDirection;
            bool foundOpponent = false;

            while (cx >= 0 && cx < boardSize && cy >= 0 && cy < boardSize){
                if(boardState[cx][cy] == nextTurn(turn)){
                    flippedPiecesInDirection.append(QPoint(cx, cy));
                    cx += dx;
                    cy += dy;
                    foundOpponent = true;
                } else if(boardState[cx][cy] == turn) {
                    if(foundOpponent){
                        canPlace = true;
                        flippedPieces += flippedPiecesInDirection;
                    }
                    break;
                } else {
                    break;
                }
            }
        }
    }
    if(canPlace){
        for(const QPoint &point : flippedPieces){
            boardState[point.x()][point.y()] = turn;
        }
        boardState[x][y] = turn;
        turn = nextTurn(turn);
        update();
    } else {
        if(!canPlacePiece(turn)){
            turn = nextTurn(turn);
            if(!canPlacePiece(turn)){
                if(isGameOver()){

                }
            }
        }
    }
}

//ターンを切り替える関数
BoardWidget::PieceColor BoardWidget::nextTurn(PieceColor currentTurn) const
{
    return (currentTurn == PieceColor::Black) ? PieceColor::White : PieceColor::Black;
}
